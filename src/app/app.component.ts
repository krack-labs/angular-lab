
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public doneCount: number = 2;
  public todos = [
    {
      "title" : "Créer un TP angular",
      "done" : true
    },
    {
      "title" : "Mettre le TP en ligne",
      "done" : true
    },
    {
      "title" : "Lancer le tp",
      "done" : true
    }
  ];
}