# Angular Lab

Le but de ce lab est une initialisation à Angular en proposant une utilisation des grands principes.

## Etape 0 : découverte du projet
Un projet angular a déjà initalisé avec une application basique de liste de todos . 

Regarder l'application web présente qui s'est ouvert lors du lancement et appréhender cette application très simple.

<details>
  <summary>Description de l'application</summary>

  - liste de todos
  - affichage d'un titre pour chaque todos
  - case à cocher pour chaque todos signifiant la réalisation ou non
  - todos barrés si fini
  - compteur de todos finis/totals
  - pas de mise à jour.
</details>

Parmis tous les fichiers du projets, voici ceux utiles pour ce lab :  

<details>
  <summary>src/app/app.component.html</summary>
  
  Contient la structure html de la page présente dans le navigateur. 

  Pour le moment, il s'agit d'une simple page html.

  Tout est codé en dur et non dynamique.
</details>

<details>
  <summary>src/app/app.component.scss</summary>
  
  Contient le style de notre composant, à savoir que la class `done` barre la ligne.
</details>

<details>
  <summary>src/app/app.component.ts</summary>
  
  Contient la définition du composant (page html, css)
  ```typescript
  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
  })
  ```

  Ainsi que les variables et fonction utiles pour le composant 
  ```typescript
    public doneCount: number = 2;
    public todos = [
      ...   
    ]
  ```
</details>

## Etape 1: utiliser la variables du compte de todos terminés
Angular permet l'[interpolation](https://angular.io/guide/interpolation) de variables. C'est a dire l'utilisation dans la partie html des variables définies dans la partie Typescript.

Remplacer le nombre de todos terminés dans la partie html par la variable `doneCount` grâce a l'interpolation


<details>
  <summary>Réponse</summary>

`src/app/app.components.html`
```HTML
  <span>done : {{ doneCount }}/3</span>  
```
</details>

> ### mots clés
>
> - interpolation
> - templating

## Etape 2 : afficher la liste des `title` des todos
Remplacer l'affichage de la liste en dur par l'usage de la variables `todos` du fichier `src/app.component.ts`.
Pour cela utilisé [*ngFor](https://guide-angular.wishtack.io/angular/composants/ngfor).

Pour le moment n'afficher que les titres des todos , SANS les cases à cocher ou le barré.

<details>
  <summary>Réponse</summary>

  `src/app/app.components.html`
  ```HTML
  <ul>
    <li *ngFor="let todo of todos">{{ todo.title }}</li>
   </ul>
  ```

</details>

> ### mots clés
>
> - templating
> - *ngFor


## Etape 3 : afficher le nombre de todos basé sur la taille de la liste
Grace à l'interpolation, remplacer le nombre de totos réalisés totals, par la longeurs de la liste.

Ajouter un nouvel élément dans la liste pour s'assurer du fonctionnement.

<details>
  <summary>Réponse</summary>

  `src/app/app.components.html`
  ```HTML
  <span>done : {{ doneCount }}/{{ todos.length }}</span> 
  ```

  `src/app/app.components.ts`
  ```typescript
    public todos = [
      {
        "title" : "Créer un TP angular",
        "done" : true
      },
      {
        "title" : "Mettre le TP en ligne",
        "done" : true
      },
      {
        "title" : "Lancer le tp",
        "done" : true
      },
      {
        "title" : "Ajouter un todo  dans la liste manuellement",
        "done" : false
      }
    ];
  ```
</details>

> ### mots clés
>
> - interpolation
> - templating


## Etape 4 : afficher les cases à cocher avec l'attribut `done`
Ajouter des cases à cocher qui doivent être cocher lorsque le champs `done` est à `true`.

<details>
  <summary>Réponse</summary>

  `src/app/app.components.html`
  ```HTML
    <li *ngFor="let todo of todos"><input type="checkbox" [(ngModel)]="todo.done"/>{{ todo.title }}</li>
  ```

</details>

> ### mots clés
>
> - templating
> - binding
> - binding bi directionnel


## Etape 5 : comprendre le mapping bi directionnel

Utiliser le code suivant, qui affiche la variable done, et ne fait que l'interprétation de celle ci dans la checkbox.

`src/app/app.components.html`
```HTML
  <li *ngFor="let todo of todos"><input type="checkbox" [ngModel]="todo.done"/>{{ todo.title }} - {{ todo.done }}</li>
```

Jouer avec les checkbox et constater le résultat sur la variable done.

Utiliser le code suivant, et jouer avec les checkbox
`src/app/app.components.html`
```HTML
  <li *ngFor="let todo of todos"><input type="checkbox" [(ngModel)]="todo.done"/>{{ todo.title }} - {{ todo.done }}</li>
```

<details>
  <summary>Explication</summary>

  Pour angular la notation [] permet l'intéprétation de la donnée, la valeur sera donc remplacer.

  La notation () permet le binding d'un évenement.  Sans (), la mise à jour n'est pas faite ainsi la variable n'est pas mise à jour.

  Pour se souvenir de l'ordre, pensé à une banane dans une boite.

</details>

> ### mots clés
>
> - binding bi directionnel


## Etape 6 : barrer les lignes en fonction de l'attribut `done`
Ajouter la classe `done` en fonction de l'attribut `done` des `todos` sur chaque `li` grace à [ngClass](https://angular.io/api/common/NgClass#description).

<details>
  <summary>Réponse</summary>

  `src/app/app.components.html`
  ```HTML
    <li *ngFor="let todo of todos" [ngClass]="{done: todo.done}"><input type="checkbox" [(ngModel)]="todo.done"/>{{ todo.title }}</li>
  ```

</details>

> ### mots clés
>
> - templating
> - style
> - ngClass

## Etape 7 : recalculer le nombre de todos réalisés
Sur les [`click`](https://angular.io/guide/event-binding#binding-to-events) sur les cases à cocher, lancer une fonction `finish` qui compte le nombre de `todos` réalisés et met à jours la variable `doneCount`

<details>
  <summary>Réponse</summary>

  `src/app/app.components.html`
  ```HTML
    <li *ngFor="let todo of todos" [ngClass]="{done: todo.done}"><input type="checkbox" [(ngModel)]="todo.done" (click)="finish()"/>{{ todo.title }}</li>
  ```

  `src/app/app.components.ts`
  ```typescript
  public finish(): void{
    this.doneCount = this.todos.filter(todo=>todo.done).length;
  }
  ```
</details>

> ### mots clés
>
> - function
> - évenement

### Instant qualification
Quels sont les 2 problèmes que vous constatez? Comment l'expliquez vous?
<details>
  <summary>Réponse</summary>

  La valeur est mal initialisé, parce qu'il n'y a pas de `click` pour intialiser la variable, elle prend donc celle par défaut.

  La valeur a toujours une différence de 1, parce que le `click` s'applique avant le changement de la varaible `done`.
</details>


## Etate 8 : initialiser le composant
Utilisé la méthode [ngOnInit](https://angular.io/api/core/OnInit#usage-notes) de l'interface `OnInit` pour calculer `doneCount` au chargement

<details>
  <summary>Réponse</summary>


  `src/app/app.components.ts`
  ```typescript
  export class AppComponent implements OnInit{
    ngOnInit(): void {
      this.finish();
    }
  ```
</details>

> ### mots clés
>
> - Cycle de vie
> - OnInit

## Etate 9 : utiliser le bon event
Utiliser l'évènement `change` plutôt que `click` pour appeler la méthode `finish`, qui s'éxecute après le changement de la variable.

<details>
  <summary>Réponse</summary>


  `src/app/app.components.html`
  ```HTML
    <li *ngFor="let todo of todos" [ngClass]="{done: todo.done}"><input type="checkbox" [(ngModel)]="todo.done" (change)="finish()"/>{{ todo.title }}</li>
  ```
</details>

> ### mots clés
>
> - évenement
> - change
> - click


## Etate 10 : Ajouter un message lors qu'il n'y a aucune tache réalisé
Utiliser [*ngIf](https://angular.io/api/common/NgIf#description) pour afficher un message lors que `doneCount` vaut 0.

<details>
  <summary>Réponse</summary>


  `src/app/app.components.html`
  ```HTML
    <span *ngIf="doneCount == 0">Il est temps de bosser... Glandu!!</span>
  ```
</details>

> ### mots clés
>
> - templating
> - *ngIf

## Etape 11 : Ajouter un composant de création de todos

Utiliser le [CLI angular](https://angular.io/cli) pour créer un composant nommé `todoAdd` dans `src/app/components`.
Et ajouter ce composant (sans le changer), à la suite de la liste de todos.

<details>
  <summary>Réponse</summary>


  `bash`
  ```bash
    npm run ng generate component components/todoAdd
  ```

  `src/app/app.components.html`
  ```HTML
    <app-todo-add></app-todo-add>
  ```
  
</details>

> ### mots clés
>
> - composant
> - cli

## Etape 12 : Ajouter un formulaire d'ajout de todos
Construiser un formulaire qui permettra de prendre un titre de `todos` et l'afficher lors de la soummission du formulaire grâce a un `alert`

<details>
  <summary>Réponse</summary>



  `src/app/components/todo-add/todo-add.component.html`
  ```HTML
    <form (submit)="add()">
      <label>
          <input type="text" name="title" [(ngModel)]="title" />
      </label>

      <input type="submit" value="Ok"/>
  </form>
  ```

  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
    export class TodoAddComponent {
      public title: String = "";
      public add(): void{
        alert(this.title);
      }
    }

  ```
  
</details>

> ### mots clés
>
> - formulaire
> - évènement
> - binding bi directionnel

## Etape 13 : Ajouter le nouveau todo dans la liste
Grace à [@Input](https://docs.angular.lat/guide/inputs-outputs#how-to-use-input), passer la liste `todos` au nouveau composant pour qu'il puisse ajouter le nouveau todos à la liste existante.

<details>
  <summary>Réponse</summary>


  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
    export class TodoAddComponent {

    @Input()
    public list: any[] = [];
    public title: String = "";
    public add(): void{
      this.list.push({
        title: this.title,
        done: false
      });
      this.title = "";
    }
  }
  ```

  `src/app/app.component.html`
  ```HTML
   <app-todo-add [list]="todos"></app-todo-add>
  ```
  
</details>

> ### mots clés
>
> - composant
> - communcation intercomposant
> - communication parent > enfant
> - @Input

## Etape 14 : Pouvoir ajouter des todos déjà terminés
Ajouter une case à cocher pour permettre de choisir le `done` dans le formulaire d'ajout


<details>
  <summary>Réponse</summary>



  `src/app/components/todo-add/todo-add.component.html`
  ```HTML
    <form (submit)="add()">
        <label>
            title
            <input type="text" name="title" [(ngModel)]="title" />
        </label>

        <label>
            Already done
            <input type="checkbox" name="alreadyDone" [(ngModel)]="alreadyDone" />
        </label>

        <input type="submit" value="Ok"/>
    </form>
  ```

  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
    export class TodoAddComponent {

      @Input()
      public list: any[] = [];
      public title: String = "";
      public alreadyDone: Boolean = false;
      public add(): void{
        this.list.push({
          title: this.title,
          done: this.alreadyDone
        });
        this.title = "";
        this.alreadyDone = false;
      }
    }
  ```

  `src/app/components/todo-add/todo-add.component.scss`
  ```scss
  label{
    display: block;
  }
  ```
  
</details>

> ### mots clés
>
> - formulaire
> - binding bi directionnel
> - évenement
> - function
> - interpolation



### Instant qualification
Quels sont le problème que vous constatez? Comment l'expliquez vous?
<details>
  <summary>Réponse</summary>
  Lors d'un ajout d'un todo, le nombre total de todos est mis à jour, mais pas le nombre de todos réalisés. Cela s'explique car la variables total est désigné par la taille de la liste, qui change lors du `push`. Par contre, la variable `doneCount`, elle dépend d'un calcule
</details>

## Etape 15 : Mettre à jour le nombre de todos réalisés lors d'un ajout
Utiliser [@Ouput](https://docs.angular.lat/guide/inputs-outputs#how-to-use-output) pour mettre à jour `doneCount` lorsqu'un évenement de création d'un todos provient de `TodoAddComponent`, 



  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
   export class TodoAddComponent {

    @Input()
    public list: any[] = [];
    @Output() 
    create = new EventEmitter<void>();
    
    public title: String = "";
    public alreadyDone: Boolean = false;
    public add(): void{
      this.list.push({
        title: this.title,
        done: this.alreadyDone
      });
      this.create.emit();
      this.title = "";
      this.alreadyDone = false;
    }
  }

  ```

  `src/app/app.component.html`
  ```HTML
    <app-todo-add [list]="todos" (create)="finish()"></app-todo-add>
  ```
</details>

> ### mots clés
>
> - composant
> - communcation intercomposant
> - communication envant > parent
> - @Output


## Etate 16 : Eviter Any
Any correspond à Object en java, il faut donc l'éviter au possible.

Créer un objets Todo qui remplacera les usages de any.
<details>
  <summary>Réponse</summary>



  `bash`
  ```bash
    npm run ng generate class model/todo
  ```

  `src/app/model/todo.ts`
  ```typescript
    export class Todo {
      constructor(public title:string, public done: boolean){

      }
  }
  ```

  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
    export class TodoAddComponent {

      @Input()
      public list: Todo[] = [];
      @Output() 
      create = new EventEmitter<void>();

      public newTodo = new Todo("", false);

      public add(): void{
        this.list.push(this.newTodo);
        this.create.emit();
        this.newTodo = new Todo("", false);
      }
  }
  ```

  `src/app/components/todo-add/todo-add.component.html`
  ```HTML
    <form (submit)="add()">
      <label>
          title
          <input type="text" name="title" [(ngModel)]="newTodo.title" />
      </label>

      <label>
          Already done
          <input type="checkbox" name="done" [(ngModel)]="newTodo.done" />
      </label>

      <input type="submit" value="Ok"/>
  </form>
  ```

  `src/app/app.component.ts`
  ```typescript
    export class AppComponent implements OnInit{

      public doneCount: number = 2;
      public todos: Todo[] = [];

      constructor(){
        this.todos.push(new Todo("Créer un TP angular", true));
        this.todos.push(new Todo("Mettre le TP en ligne", true));
        this.todos.push(new Todo("Lancer le tp", true));
        this.todos.push(new Todo("Ajouter un todo  dans la liste manuellement", false));
      }

      ngOnInit(): void {
        this.finish();
      }
      public finish(): void{
        this.doneCount = this.todos.filter(todo=>todo.done).length;
      }
      
    }
  ```


</details>

> ### mots clés
>
> - Any
> - class


## Etate 17 : Externaliser la gestion des données
Créer un `service` nommé `Api` dans `src/app/service` avec le `cli angular`. Utiliser cette classe pour créer des fonctions permettant d'encapsuler l'usage de la liste `todos`, et fait en sorte d'utiliser ses fonctions

<details>
  <summary>Réponse</summary>

  `bash`
  ```bash
    npm run ng generate service service/api
  ```

  `src/app/service/api.service.ts`
  ```typescript
      import { Injectable } from '@angular/core';
      import { Todo } from '../model/todo';

      @Injectable({
        providedIn: 'root'
      })
      export class ApiService {

        private todos: Todo[] = [];
        constructor() { 

          this.todos.push(new Todo("Créer un TP angular", true));
          this.todos.push(new Todo("Mettre le TP en ligne", true));
          this.todos.push(new Todo("Lancer le tp", true));
          this.todos.push(new Todo("Ajouter un todo  dans la liste manuellement", false));
        }

        public getTodos(): Todo[]{
          return this.todos;
        }

        public addTodo(newTodo: Todo): void{
          this.todos.push(newTodo);
        }
      }

  ```

  `src/app/components/todo-add/todo-add.component.ts`
  ```typescript
            
    export class TodoAddComponent {

      @Input()
      public list: Todo[] = [];
      @Output() 
      create = new EventEmitter<Todo>();

      public newTodo = new Todo("", false);

      public add(): void{
        this.create.emit(this.newTodo);
        this.newTodo = new Todo("", false);
      }
    }


  ```

  `src/app/app.component.ts`
  ```typescript
            
      import { Component, OnInit } from '@angular/core';
      import { Todo } from './model/todo';
      import { ApiService } from './service/api.service';

      @Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
      })
      export class AppComponent implements OnInit{

        public doneCount: number = 2;
        public todos: Todo[] = [];

        constructor(private api: ApiService){
        }

        ngOnInit(): void {
          this.todos = this.api.getTodos(); 
          this.finish();
        }
        public finish(): void{
          this.doneCount = this.todos.filter(todo=>todo.done).length;
        }

        public create(newTodo: Todo): void{
          this.api.addTodo(newTodo);
        }
      }

  ```

  `src/app/app.component.html`
  ```HTML
    <app-todo-add [list]="todos" (create)="create($event)"></app-todo-add>
  ```
</details>

> ### mots clés
>
> - service
> - http
> - passage par référence


## Instant qualification
Pourquoi cela fonction

<details>
  <summary>Réponse</summary>
  Tout simplement parce que la liste est passé par référence.
</details>

## Etate 18 : Externaliser la gestion des données dans un service http
Récupérer la liste depuis un service http via le module [http](https://angular.io/guide/http) de Angular sur le service REST présent à `/api/todos/` (GET)
Ajouter un élément à la liste via le module [http](https://angular.io/guide/http) de Angular sur l'url `/api/todos/` (POST)

<details>
  <summary>Réponse</summary>

  `src/app/service/api.service.ts`
  ```typescript
        import { HttpClient } from '@angular/common/http';
        import { Injectable } from '@angular/core';
        import { Observable } from 'rxjs';
        import { Todo } from '../model/todo';

        @Injectable({
          providedIn: 'root'
        })
        export class ApiService {
          private url = '/api/todos';

          constructor(private http: HttpClient) { }

          public getTodos(): Observable<Todo[]>{
            return this.http.get<Todo[]>(this.url);
          }

          public addTodo(newTodo: Todo): Observable<Todo>{
            return this.http.post<Todo>(this.url, newTodo);
          }
        }
  ```

  `src/app/app.component.ts`
  ```typescript
      public doneCount: number = 2;
      public todos: Todo[] = [];

      constructor(private api: ApiService){}

      ngOnInit(): void {
        this.api.getTodos().subscribe(todos=>{
          this.todos = todos;
          this.finish();
        }); 
      }

      public create(newTodo: Todo): void{
        this.api.addTodo(newTodo).subscribe(todo=>{
          this.todos.push(todo);
        })
      }
  ```
</details>

> ### mots clés
>
> - rxjs
> - Observable
> - programmation réactive


## Etate 19 : Créer une page de détail 
Grace au [routing Angular](https://guide-angular.wishtack.io/angular/routing/mise-en-place-du-routing#2.-configuration), ajouter un `composant` nommé `todoDetail` qui sera une seconde page qui affichera le `title` et le `done` passé en paramètre de l'url.

Cette page s'ouvrira lors du click sur un titre de todo.

Pour se faire, vous devrez également déplacer la liste dans un composant nommé `todoList`

<details>
  <summary>Réponse</summary>

  `bash`
  ```bash
    npm run ng generate component components/todoDetail
    npm run ng generate component components/todoList
  ```

  `src/app/components/todo-detail/todo-detail.component.html`
  ```html
        <h1>Detail</h1>
        <span>{{title}}</span>
        <input type="checkbox" [ngModel]="done" disabled="disabled"/>
  ```

  `src/app/app-routing.module.ts`
  ```typescript
      const routes: Routes = [
        {
          path: "detail", 
          component: TodoDetailComponent
        },
        {
          path: "**", 
          component: TodoListComponent
        }
      ];
  ```


  `src/app/components/todo-detail/todo-detail.component.ts`
  ```typescript
     export class TodoDetailComponent {
        public title: string = "";
        public done: boolean = false;

          constructor(route: ActivatedRoute ){
            route.queryParams.subscribe({next: (params: any) => {
              this.title = params.title;
              this.done = "true" == params.done.toLowerCase();
            }});
          }
      }
  ```

  `src/app/app.component.html`
  ```html
      <!-- header-->
      <h1>
        <img
          width="40"
          alt="Angular Logo"
          src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg=="
        />
        <span>Todos Manager</span>
      </h1>

      <router-outlet></router-outlet>
  ```

  `src/app/app.component.ts`
  ```typescript
    
      import { Component } from '@angular/core';

      @Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
      })
      export class AppComponent {


      }
  ```

  `src/app/components/todo-list/todo-list.component.html`
  ```html
        
    <span>done : {{ doneCount }}/{{ todos.length }}</span> 


    <ul>
      <li *ngFor="let todo of todos" [ngClass]="{done: todo.done}">
        <input type="checkbox" [(ngModel)]="todo.done" (change)="finish()"/>
      
        <a
        routerLink="/detail"
        [queryParams]="{title: todo.title, done: todo.done}"> {{ todo.title }}</a>

      </li>
    </ul>
    <span *ngIf="doneCount == 0">Il est temps de bosser... Glandu!!</span>
    <app-todo-add [list]="todos" (create)="create($event)"></app-todo-add>
  ```


  `src/app/components/todo-list/todo-list.component.ts`
  ```typescript
    
      export class TodoListComponent implements OnInit {

        public doneCount: number = 2;
        public todos: Todo[] = [];

        constructor(private api: ApiService){}

        ngOnInit(): void {
          this.api.getTodos().subscribe(todos=>{
            this.todos = todos;
            this.finish();
          }); 
        }

        public create(newTodo: Todo): void{
          this.api.addTodo(newTodo).subscribe(todo=>{
            this.todos.push(todo);
          })
        }

        public finish(): void {
          this.doneCount = this.todos.filter(todo => todo.done).length;
        }
      }

  ```
</details>

> ### mots clés
>
> - routing
> - composant

